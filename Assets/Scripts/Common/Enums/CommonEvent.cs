﻿namespace Common.Enums
{
    public static class CommonEvent
    {
        public const string LockUserInterface = "LockUserInterface";
        public const string UnLockUserInterface = "UnLockUserInterface";
        public const string ShowBlackScreen = "ShowBlackScreen";
        public const string ShowBlackScreenComplete = "ShowBlackScreenComplete";
        public const string FadeOutBlackScreen = "FadeOutBlackScreen";
        public const string FadeOutBlackScreenComplete = "FadeOutBlackScreenComplete";
    }
}