﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Common.Singleton
{
    public class Observer : MonoBehaviour
    {
        private static Observer _observer;
        private Dictionary<string, Dictionary<int, Delegate>> _listeners;

        private static Observer instance
        {
            get
            {
                if (_observer != null) return _observer;
                _observer = FindObjectOfType(typeof(Observer)) as Observer;
                if (_observer == null)
                    throw new Exception("There needs to be one active Observer script in your scene.");
                _observer.Init();
                return _observer;
            }
        }

        private void Init()
        {
            if (_listeners == null) _listeners = new Dictionary<string, Dictionary<int, Delegate>>();
        }

        public static bool IsNull()
        {
            return _observer == null;
        }

        public static void AddListener(string eventName, Delegate listener)
        {
            if (instance._listeners.ContainsKey(eventName))
            {
                var delegates = instance._listeners[eventName];
                if (delegates == null)
                {
                    delegates = new Dictionary<int, Delegate> {{listener.GetHashCode(), listener}};
                    instance._listeners[eventName] = delegates;
                }
                else if (!delegates.ContainsKey(listener.GetHashCode()))
                {
                    delegates.Add(listener.GetHashCode(), listener);
                }
            }
            else
            {
                var delegates = new Dictionary<int, Delegate>
                {
                    {listener.GetHashCode(), listener}
                };
                instance._listeners.Add(eventName, delegates);
            }
        }

        public static void RemoveListener(string eventName, Delegate listener)
        {
            if (!instance._listeners.ContainsKey(eventName)) return;
            var delegates = instance._listeners[eventName];
            if (delegates == null || !delegates.ContainsKey(listener.GetHashCode())) return;
            if (delegates.Remove(listener.GetHashCode()) && delegates.Count == 0)
                instance._listeners.Remove(eventName);
        }

        public static void Emit(string eventName, params object[] args)
        {
            if (!instance._listeners.ContainsKey(eventName)) return;
            var delegates = instance._listeners[eventName];
            if (delegates == null) return;
            foreach (var value in delegates.Values.ToList()) value.DynamicInvoke(args);
        }
    }
}