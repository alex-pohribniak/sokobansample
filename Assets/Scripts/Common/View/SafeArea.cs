﻿using UnityEngine;

namespace Common.View
{
    public class SafeArea : MonoBehaviour
    {
        private void Awake()
        {
            var rect = Screen.safeArea;

            var anchorMin = rect.position;
            anchorMin.x /= Screen.width;
            anchorMin.y /= Screen.height;

            var anchorMax = rect.position + rect.size;
            anchorMax.x /= Screen.width;
            anchorMax.y /= Screen.height;

            var rectTransform = GetComponent<RectTransform>();
            rectTransform.anchorMin = anchorMin;
            rectTransform.anchorMax = anchorMax;
        }
    }
}