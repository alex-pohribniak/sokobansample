﻿using Common.Enums;
using Common.Singleton;
using UnityEngine;
using UnityEngine.UI;

namespace Common.View
{
    public class UserInterfaceLocker : MonoBehaviour
    {
        private readonly SimpleDelegate _onLockUserInterface;
        private readonly SimpleDelegate _onUnlockUserInterface;

        private UserInterfaceLocker()
        {
            _onLockUserInterface = OnLock;
            _onUnlockUserInterface = OnUnLock;
        }

        private void OnEnable()
        {
            Observer.AddListener(CommonEvent.LockUserInterface, _onLockUserInterface);
            Observer.AddListener(CommonEvent.UnLockUserInterface, _onUnlockUserInterface);
        }

        private void OnDisable()
        {
            if (Observer.IsNull()) return;
            Observer.RemoveListener(CommonEvent.LockUserInterface, _onLockUserInterface);
            Observer.RemoveListener(CommonEvent.UnLockUserInterface, _onUnlockUserInterface);
        }

        private void OnUnLock()
        {
            GetComponent<GraphicRaycaster>().enabled = true;
        }

        private void OnLock()
        {
            GetComponent<GraphicRaycaster>().enabled = false;
        }
    }
}