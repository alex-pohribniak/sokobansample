﻿using UnityEngine;

namespace SokobanSample.Config
{
    public static class SokobanSampleConfig
    {
        public static readonly Vector2 CellSize = new Vector2(1.818f, 1.818f);
        
        public static readonly Vector2Int MoveTop = Vector2Int.up;
        public static readonly Vector2Int MoveBottom = Vector2Int.down;
        public static readonly Vector2Int MoveLeft = Vector2Int.left;
        public static readonly Vector2Int MoveRight = Vector2Int.right;
    }
}