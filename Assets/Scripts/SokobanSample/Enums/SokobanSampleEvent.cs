﻿namespace SokobanSample.Enums
{
    public static class SokobanSampleEvent
    {
        public const string PlayerWon = "PlayerWon";
        public const string PlayerLost = "PlayerLost";
        public const string ShowResult = "ShowResult";
        public const string ShowResultComplete = "ShowResultComplete";
        public const string BuildLevel = "BuildLevel";
        public const string UpdateView = "UpdateView";
        public const string CharacterAction = "CharacterAction";
        public const string MoveCharacter = "MoveCharacter";
        public const string MoveCharacterComplete = "MoveCharacterComplete";
        public const string MoveCubeByCharacter = "MoveCubeByCharacter";
        public const string MoveCubeByCharacterComplete = "MoveCubeByCharacterComplete";
        public const string ResetLevel = "ResetLevel";
        public const string ResetLevelView = "ResetLevelView";
        public const string DefineRotateDuration = "DefineRotateDuration";
    }
}