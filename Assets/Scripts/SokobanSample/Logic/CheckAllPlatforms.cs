﻿using System.Linq;
using Common.Singleton;
using Common.Tasks;
using SokobanSample.Enums;

namespace SokobanSample.Logic
{
    public class CheckAllPlatforms : BaseTask
    {
        protected override void OnExecute()
        {
            base.OnExecute();
            var platforms = MainModel.SokobanSampleData.platforms;
            if (platforms.All(platform => platform.activated))
            {
                MainModel.SokobanSampleData.win = true;
                Observer.Emit(SokobanSampleEvent.PlayerWon);
            }

            Complete();
        }
    }
}