﻿using System.Linq;
using Common.Singleton;
using Common.Tasks;

namespace SokobanSample.Logic
{
    public class CheckPlatform : BaseTask
    {
        protected override void OnExecute()
        {
            base.OnExecute();
            var targetCube = MainModel.SokobanSampleData.targetCube;
            var platforms = MainModel.SokobanSampleData.platforms;
            var targetPlatform =
                platforms.FirstOrDefault(platform => platform.platformPosition == targetCube.cubePosition);
            if (targetPlatform != null) targetPlatform.activated = true;

            var moveDirection = MainModel.SokobanSampleData.moveDirection;
            var targetCubePreviousPosition = targetCube.cubePosition - moveDirection;
            var previousPlatform =
                platforms.FirstOrDefault(platform => platform.platformPosition == targetCubePreviousPosition);
            if (previousPlatform != null) previousPlatform.activated = false;

            Complete();
        }
    }
}