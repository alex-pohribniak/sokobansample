﻿using Common.Singleton;
using Common.Tasks;

namespace SokobanSample.Logic
{
    public class CheckPossibleAction : BaseTask
    {
        public BaseTask moveCharacter;
        public BaseTask moveCubeByCharacter;

        protected override void OnExecute()
        {
            base.OnExecute();
            var character = MainModel.SokobanSampleData.character;
            var moveDirection = MainModel.SokobanSampleData.moveDirection;

            if (character.IsWallExistsTowards(moveDirection) == false)
            {
                if (character.IsCubeExistsTowards(moveDirection) == false)
                {
                    moveCharacter.Execute();
                }
                else
                {
                    var targetCube = MainModel.SokobanSampleData.targetCube;
                    if (targetCube.IsWallExistsTowards(moveDirection) == false &&
                        targetCube.IsCubeExistsTowards(moveDirection) == false)
                        moveCubeByCharacter.Execute();
                    else
                        Complete();
                }
            }
            else
            {
                Complete();
            }
        }

        protected override void Activate()
        {
            base.Activate();
            moveCharacter.onTaskComplete = task => { Complete(); };
            moveCubeByCharacter.onTaskComplete = task => { Complete(); };
        }

        protected override void DeActivate()
        {
            base.DeActivate();
            moveCharacter.onTaskComplete = null;
            moveCubeByCharacter.onTaskComplete = null;
        }
    }
}