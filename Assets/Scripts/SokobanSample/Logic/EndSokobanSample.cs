﻿using Common.Tasks;
using UnityEngine.SceneManagement;

namespace SokobanSample.Logic
{
    public class EndSokobanSample : BaseTask
    {
        protected override void OnExecute()
        {
            base.OnExecute();
            SceneManager.LoadScene("SokobanSample");
            Complete();
        }
    }
}