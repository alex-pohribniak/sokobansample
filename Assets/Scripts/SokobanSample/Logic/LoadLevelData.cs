﻿using System.Collections.Generic;
using Common.Singleton;
using Common.Tasks;
using SokobanSample.Model;
using UnityEngine;

namespace SokobanSample.Logic
{
    public class LoadLevelData : BaseTask
    {
        protected override void OnExecute()
        {
            base.OnExecute();
            MainModel.SokobanSampleData.walls = new Dictionary<Vector2Int, WallData>();
            var wallData = new WallData {bottom = true, left = true};
            MainModel.SokobanSampleData.walls.Add(new Vector2Int(-5, -5), wallData);
            wallData = new WallData {left = true};
            MainModel.SokobanSampleData.walls.Add(new Vector2Int(-5, -4), wallData);
            wallData = new WallData {top = true, left = true};
            MainModel.SokobanSampleData.walls.Add(new Vector2Int(-5, -3), wallData);
            wallData = new WallData {bottom = true, left = true};
            MainModel.SokobanSampleData.walls.Add(new Vector2Int(-5, -2), wallData);
            wallData = new WallData {left = true};
            MainModel.SokobanSampleData.walls.Add(new Vector2Int(-5, -1), wallData);
            wallData = new WallData {top = true, left = true};
            MainModel.SokobanSampleData.walls.Add(new Vector2Int(-5, 0), wallData);
            wallData = new WallData {bottom = true, left = true};
            MainModel.SokobanSampleData.walls.Add(new Vector2Int(-5, 1), wallData);
            wallData = new WallData {top = true, left = true};
            MainModel.SokobanSampleData.walls.Add(new Vector2Int(-5, 2), wallData);
            wallData = new WallData {bottom = true, left = true};
            MainModel.SokobanSampleData.walls.Add(new Vector2Int(-5, 3), wallData);
            wallData = new WallData {left = true};
            MainModel.SokobanSampleData.walls.Add(new Vector2Int(-5, 4), wallData);
            wallData = new WallData {top = true, left = true};
            MainModel.SokobanSampleData.walls.Add(new Vector2Int(-5, 5), wallData);
            wallData = new WallData {top = true};
            MainModel.SokobanSampleData.walls.Add(new Vector2Int(-4, 5), wallData);
            wallData = new WallData {top = true, right = true};
            MainModel.SokobanSampleData.walls.Add(new Vector2Int(-3, 5), wallData);
            wallData = new WallData {top = true, left = true};
            MainModel.SokobanSampleData.walls.Add(new Vector2Int(-2, 5), wallData);
            wallData = new WallData {top = true};
            MainModel.SokobanSampleData.walls.Add(new Vector2Int(-1, 5), wallData);
            wallData = new WallData {top = true};
            MainModel.SokobanSampleData.walls.Add(new Vector2Int(0, 5), wallData);
            wallData = new WallData {top = true};
            MainModel.SokobanSampleData.walls.Add(new Vector2Int(1, 5), wallData);
            wallData = new WallData {top = true, right = true};
            MainModel.SokobanSampleData.walls.Add(new Vector2Int(2, 5), wallData);
            wallData = new WallData {top = true, left = true};
            MainModel.SokobanSampleData.walls.Add(new Vector2Int(3, 5), wallData);
            wallData = new WallData {top = true};
            MainModel.SokobanSampleData.walls.Add(new Vector2Int(4, 5), wallData);
            wallData = new WallData {top = true, right = true};
            MainModel.SokobanSampleData.walls.Add(new Vector2Int(5, 5), wallData);
            wallData = new WallData {right = true};
            MainModel.SokobanSampleData.walls.Add(new Vector2Int(5, 4), wallData);
            wallData = new WallData {bottom = true, right = true};
            MainModel.SokobanSampleData.walls.Add(new Vector2Int(5, 3), wallData);
            wallData = new WallData {top = true, right = true};
            MainModel.SokobanSampleData.walls.Add(new Vector2Int(5, 2), wallData);
            wallData = new WallData {bottom = true, right = true};
            MainModel.SokobanSampleData.walls.Add(new Vector2Int(5, 1), wallData);
            wallData = new WallData {top = true, right = true};
            MainModel.SokobanSampleData.walls.Add(new Vector2Int(5, 0), wallData);
            wallData = new WallData {right = true};
            MainModel.SokobanSampleData.walls.Add(new Vector2Int(5, -1), wallData);
            wallData = new WallData {bottom = true, right = true};
            MainModel.SokobanSampleData.walls.Add(new Vector2Int(5, -2), wallData);
            wallData = new WallData {top = true, right = true};
            MainModel.SokobanSampleData.walls.Add(new Vector2Int(5, -3), wallData);
            wallData = new WallData {right = true};
            MainModel.SokobanSampleData.walls.Add(new Vector2Int(5, -4), wallData);
            wallData = new WallData {bottom = true, right = true};
            MainModel.SokobanSampleData.walls.Add(new Vector2Int(5, -5), wallData);
            wallData = new WallData {bottom = true};
            MainModel.SokobanSampleData.walls.Add(new Vector2Int(4, -5), wallData);
            wallData = new WallData {bottom = true, left = true};
            MainModel.SokobanSampleData.walls.Add(new Vector2Int(3, -5), wallData);
            wallData = new WallData {bottom = true, right = true};
            MainModel.SokobanSampleData.walls.Add(new Vector2Int(2, -5), wallData);
            wallData = new WallData {bottom = true};
            MainModel.SokobanSampleData.walls.Add(new Vector2Int(1, -5), wallData);
            wallData = new WallData {bottom = true};
            MainModel.SokobanSampleData.walls.Add(new Vector2Int(0, -5), wallData);
            wallData = new WallData {bottom = true};
            MainModel.SokobanSampleData.walls.Add(new Vector2Int(-1, -5), wallData);
            wallData = new WallData {bottom = true, left = true};
            MainModel.SokobanSampleData.walls.Add(new Vector2Int(-2, -5), wallData);
            wallData = new WallData {bottom = true, right = true};
            MainModel.SokobanSampleData.walls.Add(new Vector2Int(-3, -5), wallData);
            wallData = new WallData {bottom = true};
            MainModel.SokobanSampleData.walls.Add(new Vector2Int(-4, -5), wallData);

            wallData = new WallData {top = true, right = true};
            MainModel.SokobanSampleData.walls.Add(new Vector2Int(-3, -3), wallData);
            wallData = new WallData {left = true};
            MainModel.SokobanSampleData.walls.Add(new Vector2Int(-2, -3), wallData);
            wallData = new WallData {bottom = true};
            MainModel.SokobanSampleData.walls.Add(new Vector2Int(-3, -2), wallData);
            wallData = new WallData {top = true};
            MainModel.SokobanSampleData.walls.Add(new Vector2Int(-4, -3), wallData);
            wallData = new WallData {bottom = true};
            MainModel.SokobanSampleData.walls.Add(new Vector2Int(-4, -2), wallData);

            wallData = new WallData {right = true};
            MainModel.SokobanSampleData.walls.Add(new Vector2Int(2, -3), wallData);
            wallData = new WallData {top = true, left = true};
            MainModel.SokobanSampleData.walls.Add(new Vector2Int(3, -3), wallData);
            wallData = new WallData {bottom = true};
            MainModel.SokobanSampleData.walls.Add(new Vector2Int(3, -2), wallData);
            wallData = new WallData {top = true};
            MainModel.SokobanSampleData.walls.Add(new Vector2Int(4, -3), wallData);
            wallData = new WallData {bottom = true};
            MainModel.SokobanSampleData.walls.Add(new Vector2Int(4, -2), wallData);

            wallData = new WallData {bottom = true};
            MainModel.SokobanSampleData.walls.Add(new Vector2Int(1, -3), wallData);
            wallData = new WallData {bottom = true};
            MainModel.SokobanSampleData.walls.Add(new Vector2Int(0, -3), wallData);
            wallData = new WallData {bottom = true};
            MainModel.SokobanSampleData.walls.Add(new Vector2Int(-1, -3), wallData);
            wallData = new WallData {top = true};
            MainModel.SokobanSampleData.walls.Add(new Vector2Int(1, -4), wallData);
            wallData = new WallData {top = true};
            MainModel.SokobanSampleData.walls.Add(new Vector2Int(0, -4), wallData);
            wallData = new WallData {top = true};
            MainModel.SokobanSampleData.walls.Add(new Vector2Int(-1, -4), wallData);

            wallData = new WallData {left = true, right = true};
            MainModel.SokobanSampleData.walls.Add(new Vector2Int(0, -2), wallData);
            wallData = new WallData {right = true};
            MainModel.SokobanSampleData.walls.Add(new Vector2Int(-1, -2), wallData);
            wallData = new WallData {left = true};
            MainModel.SokobanSampleData.walls.Add(new Vector2Int(1, -2), wallData);
            wallData = new WallData {left = true, right = true};
            MainModel.SokobanSampleData.walls.Add(new Vector2Int(0, -1), wallData);
            wallData = new WallData {top = true, right = true};
            MainModel.SokobanSampleData.walls.Add(new Vector2Int(-1, -1), wallData);
            wallData = new WallData {top = true, left = true};
            MainModel.SokobanSampleData.walls.Add(new Vector2Int(1, -1), wallData);

            wallData = new WallData {bottom = true};
            MainModel.SokobanSampleData.walls.Add(new Vector2Int(-1, 0), wallData);
            wallData = new WallData {bottom = true};
            MainModel.SokobanSampleData.walls.Add(new Vector2Int(-2, 0), wallData);
            wallData = new WallData {top = true, bottom = true};
            MainModel.SokobanSampleData.walls.Add(new Vector2Int(-3, 0), wallData);
            wallData = new WallData {top = true};
            MainModel.SokobanSampleData.walls.Add(new Vector2Int(-2, -1), wallData);
            wallData = new WallData {top = true};
            MainModel.SokobanSampleData.walls.Add(new Vector2Int(-3, -1), wallData);

            wallData = new WallData {bottom = true};
            MainModel.SokobanSampleData.walls.Add(new Vector2Int(1, 0), wallData);
            wallData = new WallData {bottom = true};
            MainModel.SokobanSampleData.walls.Add(new Vector2Int(2, 0), wallData);
            wallData = new WallData {top = true, bottom = true};
            MainModel.SokobanSampleData.walls.Add(new Vector2Int(3, 0), wallData);
            wallData = new WallData {top = true};
            MainModel.SokobanSampleData.walls.Add(new Vector2Int(2, -1), wallData);
            wallData = new WallData {top = true};
            MainModel.SokobanSampleData.walls.Add(new Vector2Int(3, -1), wallData);

            wallData = new WallData {top = true};
            MainModel.SokobanSampleData.walls.Add(new Vector2Int(-3, 2), wallData);
            wallData = new WallData {right = true, bottom = true};
            MainModel.SokobanSampleData.walls.Add(new Vector2Int(-3, 3), wallData);
            wallData = new WallData {left = true, right = true};
            MainModel.SokobanSampleData.walls.Add(new Vector2Int(-2, 3), wallData);

            wallData = new WallData {top = true};
            MainModel.SokobanSampleData.walls.Add(new Vector2Int(3, 2), wallData);
            wallData = new WallData {left = true, bottom = true};
            MainModel.SokobanSampleData.walls.Add(new Vector2Int(3, 3), wallData);
            wallData = new WallData {right = true, left = true};
            MainModel.SokobanSampleData.walls.Add(new Vector2Int(2, 3), wallData);

            wallData = new WallData {top = true, bottom = true};
            MainModel.SokobanSampleData.walls.Add(new Vector2Int(-1, 2), wallData);
            wallData = new WallData {bottom = true, left = true};
            MainModel.SokobanSampleData.walls.Add(new Vector2Int(-1, 3), wallData);

            wallData = new WallData {top = true, bottom = true};
            MainModel.SokobanSampleData.walls.Add(new Vector2Int(1, 2), wallData);
            wallData = new WallData {bottom = true, right = true};
            MainModel.SokobanSampleData.walls.Add(new Vector2Int(1, 3), wallData);

            wallData = new WallData {bottom = true};
            MainModel.SokobanSampleData.walls.Add(new Vector2Int(-3, 1), wallData);
            wallData = new WallData {top = true};
            MainModel.SokobanSampleData.walls.Add(new Vector2Int(-4, 0), wallData);
            wallData = new WallData {bottom = true};
            MainModel.SokobanSampleData.walls.Add(new Vector2Int(-4, 1), wallData);

            wallData = new WallData {bottom = true};
            MainModel.SokobanSampleData.walls.Add(new Vector2Int(3, 1), wallData);
            wallData = new WallData {top = true};
            MainModel.SokobanSampleData.walls.Add(new Vector2Int(4, 0), wallData);
            wallData = new WallData {bottom = true};
            MainModel.SokobanSampleData.walls.Add(new Vector2Int(4, 1), wallData);

            wallData = new WallData {left = true, right = true};
            MainModel.SokobanSampleData.walls.Add(new Vector2Int(0, 1), wallData);
            wallData = new WallData {right = true, top = true};
            MainModel.SokobanSampleData.walls.Add(new Vector2Int(-1, 1), wallData);
            wallData = new WallData {left = true, top = true};
            MainModel.SokobanSampleData.walls.Add(new Vector2Int(1, 1), wallData);

            MainModel.SokobanSampleData.platforms = new List<PlatformData>();
            MainModel.SokobanSampleData.platforms.Add(new PlatformData {platformPosition = new Vector2Int(-4, 4)});
            MainModel.SokobanSampleData.platforms.Add(new PlatformData {platformPosition = new Vector2Int(0, 4)});
            MainModel.SokobanSampleData.platforms.Add(new PlatformData {platformPosition = new Vector2Int(4, 4)});

            MainModel.SokobanSampleData.cubes = new List<CubeData>();
            MainModel.SokobanSampleData.cubes.Add(new CubeData {cubePosition = new Vector2Int(-4, -4)});
            MainModel.SokobanSampleData.cubes.Add(new CubeData {cubePosition = new Vector2Int(0, -4)});
            MainModel.SokobanSampleData.cubes.Add(new CubeData {cubePosition = new Vector2Int(4, -4)});
            foreach (var cube in MainModel.SokobanSampleData.cubes) cube.initialPosition = cube.cubePosition;

            MainModel.SokobanSampleData.character = new CharacterData();
            MainModel.SokobanSampleData.character.characterPosition = Vector2Int.zero;
            MainModel.SokobanSampleData.character.initialPosition =
                MainModel.SokobanSampleData.character.characterPosition;
            Complete();
        }
    }
}