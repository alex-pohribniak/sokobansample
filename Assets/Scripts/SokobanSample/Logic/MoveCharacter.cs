﻿using Common.Enums;
using Common.Singleton;
using Common.Tasks;
using SokobanSample.Enums;

namespace SokobanSample.Logic
{
    public class MoveCharacter : BaseTask
    {
        private readonly SimpleDelegate _onMoveCharacterComplete;

        public MoveCharacter()
        {
            _onMoveCharacterComplete = Complete;
        }

        protected override void OnExecute()
        {
            base.OnExecute();
            MainModel.SokobanSampleData.character.characterPosition += MainModel.SokobanSampleData.moveDirection;
            MainModel.SokobanSampleData.character.moves++;
            if (MainModel.SokobanSampleData.targetCube != null) MainModel.SokobanSampleData.character.pushes++;
            Observer.Emit(SokobanSampleEvent.MoveCharacter);
        }

        protected override void Activate()
        {
            base.Activate();
            Observer.AddListener(SokobanSampleEvent.MoveCharacterComplete, _onMoveCharacterComplete);
        }

        protected override void DeActivate()
        {
            base.DeActivate();
            if (Observer.IsNull()) return;
            Observer.RemoveListener(SokobanSampleEvent.MoveCharacterComplete, _onMoveCharacterComplete);
        }
    }
}