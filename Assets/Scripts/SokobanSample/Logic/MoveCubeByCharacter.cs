﻿using Common.Enums;
using Common.Singleton;
using Common.Tasks;
using SokobanSample.Enums;

namespace SokobanSample.Logic
{
    public class MoveCubeByCharacter : BaseTask
    {
        private readonly SimpleDelegate _onMoveCubeByCharacterComplete;

        public MoveCubeByCharacter()
        {
            _onMoveCubeByCharacterComplete = Complete;
        }

        protected override void OnExecute()
        {
            base.OnExecute();
            MainModel.SokobanSampleData.targetCube.cubePosition += MainModel.SokobanSampleData.moveDirection;
            Observer.Emit(SokobanSampleEvent.MoveCubeByCharacter);
        }

        protected override void Activate()
        {
            base.Activate();
            Observer.AddListener(SokobanSampleEvent.MoveCubeByCharacterComplete, _onMoveCubeByCharacterComplete);
        }

        protected override void DeActivate()
        {
            base.DeActivate();
            if (Observer.IsNull()) return;
            Observer.RemoveListener(SokobanSampleEvent.MoveCubeByCharacterComplete, _onMoveCubeByCharacterComplete);
        }
    }
}