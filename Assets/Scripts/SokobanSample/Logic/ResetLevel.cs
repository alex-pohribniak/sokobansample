﻿using Common.Singleton;
using Common.Tasks;
using SokobanSample.Enums;

namespace SokobanSample.Logic
{
    public class ResetLevel : BaseTask
    {
        protected override void OnExecute()
        {
            base.OnExecute();
            var cubes = MainModel.SokobanSampleData.cubes;
            foreach (var cube in cubes) cube.cubePosition = cube.initialPosition;
            var character = MainModel.SokobanSampleData.character;
            character.characterPosition = character.initialPosition;
            character.moves = 0;
            character.pushes = 0;
            var platforms = MainModel.SokobanSampleData.platforms;
            foreach (var platform in platforms) platform.activated = false;
            Observer.Emit(SokobanSampleEvent.ResetLevelView);
            Complete();
        }
    }
}