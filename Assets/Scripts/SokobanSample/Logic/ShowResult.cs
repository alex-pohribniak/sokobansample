﻿using Common.Enums;
using Common.Singleton;
using Common.Tasks;
using SokobanSample.Enums;

namespace SokobanSample.Logic
{
    public class ShowResult : BaseTask
    {
        private readonly SimpleDelegate _onShowResultComplete;

        public ShowResult()
        {
            _onShowResultComplete = Complete;
        }

        protected override void OnExecute()
        {
            base.OnExecute();
            Observer.Emit(SokobanSampleEvent.ShowResult);
        }

        protected override void Activate()
        {
            base.Activate();
            Observer.AddListener(SokobanSampleEvent.ShowResultComplete, _onShowResultComplete);
        }

        protected override void DeActivate()
        {
            base.DeActivate();
            if (Observer.IsNull()) return;
            Observer.RemoveListener(SokobanSampleEvent.ShowResultComplete, _onShowResultComplete);
        }
    }
}