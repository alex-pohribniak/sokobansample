﻿using Common.Enums;
using Common.Singleton;
using Common.Tasks;
using SokobanSample.Enums;
using UnityEngine;

namespace SokobanSample.Logic
{
    public class SokobanSampleLogic : MonoBehaviour
    {
        public BaseTask endSokobanSample;
        public BaseTask characterAction;
        public BaseTask startTask;
        public BaseTask resetLevel;
        
        private readonly SimpleDelegate _onCharacterAction;
        private readonly FloatDelegate _onDefineRotateDuration;
        private readonly SimpleDelegate _onPlayerLost;
        private readonly SimpleDelegate _onPlayerWon;
        private readonly SimpleDelegate _onResetLevel;

        public SokobanSampleLogic()
        {
            _onDefineRotateDuration = OnDefineRotateDuration;
            _onResetLevel = OnResetLevel;
            _onCharacterAction = OnCharacterAction;
            _onPlayerLost = OnPlayerLost;
            _onPlayerWon = OnPlayerWon;
        }

        private void Awake()
        {
            Observer.AddListener(SokobanSampleEvent.PlayerWon, _onPlayerWon);
            Observer.AddListener(SokobanSampleEvent.PlayerLost, _onPlayerLost);
            Observer.AddListener(SokobanSampleEvent.CharacterAction, _onCharacterAction);
            Observer.AddListener(SokobanSampleEvent.ResetLevel, _onResetLevel);
            Observer.AddListener(SokobanSampleEvent.DefineRotateDuration, _onDefineRotateDuration);
        }

        private void Start()
        {
            startTask.Execute();
        }

        private void OnDestroy()
        {
            if (Observer.IsNull()) return;
            Observer.RemoveListener(SokobanSampleEvent.PlayerWon, _onPlayerWon);
            Observer.RemoveListener(SokobanSampleEvent.PlayerLost, _onPlayerLost);
            Observer.RemoveListener(SokobanSampleEvent.CharacterAction, _onCharacterAction);
            Observer.RemoveListener(SokobanSampleEvent.ResetLevel, _onResetLevel);
            Observer.RemoveListener(SokobanSampleEvent.DefineRotateDuration, _onDefineRotateDuration);
        }

        private void OnDefineRotateDuration(float rotationDuration)
        {
            var cubes = MainModel.SokobanSampleData.cubes;
            foreach (var cube in cubes) cube.moveDelayDuration = rotationDuration;
        }

        private void OnResetLevel()
        {
            resetLevel.Execute();
        }

        private void OnCharacterAction()
        {
            characterAction.Execute();
        }

        private void OnPlayerLost()
        {
            endSokobanSample.Execute();
        }

        private void OnPlayerWon()
        {
            endSokobanSample.Execute();
        }
    }
}