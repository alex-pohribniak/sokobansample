﻿using Common.Singleton;
using Common.Tasks;
using SokobanSample.Enums;

namespace SokobanSample.Logic
{
    public class UpdateView : BaseTask
    {
        protected override void OnExecute()
        {
            base.OnExecute();
            Observer.Emit(SokobanSampleEvent.UpdateView);
            Complete();
        }
    }
}