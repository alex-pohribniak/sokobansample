﻿using System;
using System.Linq;
using Common.Singleton;
using SokobanSample.Config;
using SokobanSample.Model.Interfaces;
using UnityEngine;

namespace SokobanSample.Model
{
    [Serializable]
    public class CharacterData : IObstacleChecker
    {
        public Vector2Int characterPosition;
        public Vector2Int initialPosition;
        public int moves;
        public int pushes;

        public bool IsWallExistsTowards(Vector2Int direction)
        {
            var walls = MainModel.SokobanSampleData.walls;
            var characterNextPosition = characterPosition + direction;
            if (direction == SokobanSampleConfig.MoveTop)
            {
                if (walls.ContainsKey(characterPosition) && walls[characterPosition].top ||
                    walls.ContainsKey(characterNextPosition) && walls[characterNextPosition].bottom) return true;
            }
            else if (direction == SokobanSampleConfig.MoveBottom)
            {
                if (walls.ContainsKey(characterPosition) && walls[characterPosition].bottom ||
                    walls.ContainsKey(characterNextPosition) && walls[characterNextPosition].top) return true;
            }
            else if (direction == SokobanSampleConfig.MoveLeft)
            {
                if (walls.ContainsKey(characterPosition) && walls[characterPosition].left ||
                    walls.ContainsKey(characterNextPosition) && walls[characterNextPosition].right) return true;
            }
            else if (direction == SokobanSampleConfig.MoveRight)
            {
                if (walls.ContainsKey(characterPosition) && walls[characterPosition].right ||
                    walls.ContainsKey(characterNextPosition) && walls[characterNextPosition].left) return true;
            }

            return default;
        }

        public bool IsCubeExistsTowards(Vector2Int direction)
        {
            var cubes = MainModel.SokobanSampleData.cubes;
            var characterNextPosition = characterPosition + direction;
            var targetCube = cubes.FirstOrDefault(cube => cube.cubePosition == characterNextPosition);

            if (targetCube != null)
            {
                MainModel.SokobanSampleData.targetCube = targetCube;
                return true;
            }

            MainModel.SokobanSampleData.targetCube = null;
            return false;
        }
    }
}