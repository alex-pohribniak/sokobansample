﻿using System;
using System.Linq;
using Common.Singleton;
using SokobanSample.Config;
using SokobanSample.Model.Interfaces;
using UnityEngine;

namespace SokobanSample.Model
{
    [Serializable]
    public class CubeData: IObstacleChecker
    {
        public Vector2Int cubePosition;
        public Vector2Int initialPosition;
        public float moveDelayDuration;

        public bool IsWallExistsTowards(Vector2Int direction)
        {
            var walls = MainModel.SokobanSampleData.walls;
            var cubeNextPosition = cubePosition + direction;
            if (direction == SokobanSampleConfig.MoveTop)
            {
                if (walls.ContainsKey(cubePosition) && walls[cubePosition].top ||
                    walls.ContainsKey(cubeNextPosition) && walls[cubeNextPosition].bottom) return true;
            }
            else if (direction == SokobanSampleConfig.MoveBottom)
            {
                if (walls.ContainsKey(cubePosition) && walls[cubePosition].bottom ||
                    walls.ContainsKey(cubeNextPosition) && walls[cubeNextPosition].top) return true;
            }
            else if (direction == SokobanSampleConfig.MoveLeft)
            {
                if (walls.ContainsKey(cubePosition) && walls[cubePosition].left ||
                    walls.ContainsKey(cubeNextPosition) && walls[cubeNextPosition].right) return true;
            }
            else if (direction == SokobanSampleConfig.MoveRight)
            {
                if (walls.ContainsKey(cubePosition) && walls[cubePosition].right ||
                    walls.ContainsKey(cubeNextPosition) && walls[cubeNextPosition].left) return true;
            }

            return default;
        }

        public bool IsCubeExistsTowards(Vector2Int direction)
        {
            var cubes = MainModel.SokobanSampleData.cubes;
            var cubeNextPosition = cubePosition + direction;
            return cubes.FirstOrDefault(cube => cube.cubePosition == cubeNextPosition) != null;
        }
    }
}