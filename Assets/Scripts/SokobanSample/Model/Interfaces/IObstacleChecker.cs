﻿using UnityEngine;

namespace SokobanSample.Model.Interfaces
{
    public interface IObstacleChecker
    {
        bool IsWallExistsTowards(Vector2Int direction);

        bool IsCubeExistsTowards(Vector2Int direction);
    }
}