﻿using System;
using UnityEngine;

namespace SokobanSample.Model
{
    [Serializable]
    public class PlatformData
    {
        public Vector2Int platformPosition;
        public bool activated;
    }
}