﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace SokobanSample.Model
{
    [Serializable]
    public class SokobanSampleData
    {
        public Dictionary<Vector2Int, WallData> walls;
        public List<PlatformData> platforms;
        public List<CubeData> cubes;
        public CharacterData character;
        public CubeData targetCube;
        public Vector2Int moveDirection;
        public bool win;
    }
}