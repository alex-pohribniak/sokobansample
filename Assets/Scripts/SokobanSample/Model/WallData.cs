﻿using System;

namespace SokobanSample.Model
{
    [Serializable]
    public class WallData
    {
        public bool top;
        public bool bottom;
        public bool left;
        public bool right;
    }
}