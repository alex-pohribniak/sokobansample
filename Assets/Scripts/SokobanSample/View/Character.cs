﻿using Common.Singleton;
using DG.Tweening;
using SokobanSample.Config;
using SokobanSample.Enums;
using SokobanSample.Model;
using UnityEngine;

namespace SokobanSample.View
{
    public class Character : MonoBehaviour
    {
        private const float MoveDuration = 0.5f;
        private const float RotateDuration = 0.2f;
        private static float _baseY;
        private static readonly int RunInPlace = Animator.StringToHash("RunInPlace");
        private static readonly int SpeedMultiplier = Animator.StringToHash("SpeedMultiplier");

        public GameObject avatar;
        public CharacterData data;

        private Vector3 _currentRotationVector;
        private float _moveDuration;
        private float _rotateDuration;
        private Sequence _tweenAnimation;

        private void Awake()
        {
            _baseY = transform.position.y;
            _currentRotationVector = Vector3.zero;
            _moveDuration = MoveDuration;
            _rotateDuration = RotateDuration;
        }

        private void Update()
        {
            avatar.transform.localPosition = Vector3.zero;
        }

        private void OnDestroy()
        {
            ClearTween();
        }

        public void MoveCharacter()
        {
            ClearTween();
            _tweenAnimation = DOTween.Sequence();
            DefineAnimationSpeed();
            var cellCoordinate = data.characterPosition * SokobanSampleConfig.CellSize;
            var targetPosition = new Vector3(cellCoordinate.x, _baseY, cellCoordinate.y);
            DefineRotation();
            _tweenAnimation.Append(gameObject.transform.DOMove(targetPosition, _moveDuration));
            _tweenAnimation.OnComplete(() =>
            {
                avatar.GetComponent<Animator>().SetBool(RunInPlace, false);
                Observer.Emit(SokobanSampleEvent.MoveCharacterComplete);
            });
            avatar.GetComponent<Animator>().SetBool(RunInPlace, true);
            _tweenAnimation.Play();
        }

        private void DefineAnimationSpeed()
        {
            var targetCube = MainModel.SokobanSampleData.targetCube;
            if (targetCube != null)
            {
                avatar.GetComponent<Animator>().SetFloat(SpeedMultiplier, 1f);
                _moveDuration = MoveDuration;
            }
            else
            {
                avatar.GetComponent<Animator>().SetFloat(SpeedMultiplier, 2f);
                _moveDuration = MoveDuration / 2f;
            }
        }

        private void DefineRotation()
        {
            var rotationVector = DefineRotationVector();
            if (rotationVector != _currentRotationVector)
                _tweenAnimation.Append(gameObject.transform.DORotate(rotationVector, _rotateDuration));
            else
                _rotateDuration = 0f;
            _currentRotationVector = rotationVector;
            Observer.Emit(SokobanSampleEvent.DefineRotateDuration, _rotateDuration);
        }

        private Vector3 DefineRotationVector()
        {
            var moveDirection = MainModel.SokobanSampleData.moveDirection;
            if (moveDirection == SokobanSampleConfig.MoveTop)
                return new Vector3(0f, 0f, 0f);
            if (moveDirection == SokobanSampleConfig.MoveBottom)
                return new Vector3(0f, 180f, 0f);
            if (moveDirection == SokobanSampleConfig.MoveLeft)
                return new Vector3(0f, 270f, 0f);
            return moveDirection == SokobanSampleConfig.MoveRight
                ? new Vector3(0f, 90f, 0f)
                : default;
        }

        private void ClearTween()
        {
            if (_tweenAnimation == null) return;
            _tweenAnimation.Kill();
            _tweenAnimation.onComplete = null;
            _tweenAnimation = null;
        }

        public void ResetCharacter()
        {
            var cellCoordinate = data.characterPosition * SokobanSampleConfig.CellSize;
            var targetPosition = new Vector3(cellCoordinate.x, _baseY, cellCoordinate.y);
            var obj = gameObject;
            obj.transform.position = targetPosition;
            obj.transform.eulerAngles = Vector3.zero;
        }
    }
}