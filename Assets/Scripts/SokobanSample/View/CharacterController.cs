﻿using System;
using Common.Singleton;
using SokobanSample.Config;
using SokobanSample.Enums;
using UnityEngine;
using UnityEngine.EventSystems;

namespace SokobanSample.View
{
    public class CharacterController : MonoBehaviour, IPointerDownHandler
    {
        public Character character;
        public Camera uiCamera;
        public Camera sceneCamera;
        private Vector3 _screenZeroOffset;

        private void Awake()
        {
            _screenZeroOffset = new Vector3(Screen.width / 2f, Screen.height / 2f, 0f);
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            var pointerPosition = uiCamera.WorldToScreenPoint(eventData.pointerPressRaycast.worldPosition) - _screenZeroOffset;
            var characterScreenPosition = sceneCamera.WorldToScreenPoint(character.gameObject.transform.position) - _screenZeroOffset;
            DefineDirection(pointerPosition - characterScreenPosition);
            Observer.Emit(SokobanSampleEvent.CharacterAction);
        }

        private void DefineDirection(Vector3 pointerPosition)
        {
            if (Math.Abs(pointerPosition.x) > Math.Abs(pointerPosition.y))
                MainModel.SokobanSampleData.moveDirection =
                    pointerPosition.x > 0 ? SokobanSampleConfig.MoveRight : SokobanSampleConfig.MoveLeft;
            else
                MainModel.SokobanSampleData.moveDirection =
                    pointerPosition.y > 0 ? SokobanSampleConfig.MoveTop : SokobanSampleConfig.MoveBottom;
        }
    }
}