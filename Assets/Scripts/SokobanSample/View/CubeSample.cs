﻿using Common.Singleton;
using DG.Tweening;
using SokobanSample.Config;
using SokobanSample.Enums;
using SokobanSample.Model;
using UnityEngine;

namespace SokobanSample.View
{
    public class CubeSample : MonoBehaviour
    {
        private const float MoveDelayDuration = 0.35f;
        private const float MoveDuration = 0.5f;
        private static float _baseY;

        public CubeData data;

        private float _moveDelayDuration;
        private float _moveDuration;
        private Sequence _tweenAnimation;

        private void Awake()
        {
            _baseY = transform.position.y;
            _moveDuration = MoveDuration;
        }

        private void OnDestroy()
        {
            ClearTween();
        }

        public void MoveCube()
        {
            ClearTween();
            _tweenAnimation = DOTween.Sequence();
            var cellCoordinate = data.cubePosition * SokobanSampleConfig.CellSize;
            var vector3 = new Vector3(cellCoordinate.x, _baseY, cellCoordinate.y);
            DefineMoveDelayDuration();
            _tweenAnimation.AppendInterval(_moveDelayDuration);
            _tweenAnimation.Append(gameObject.transform.DOMove(vector3, _moveDuration));
            _tweenAnimation.SetEase(Ease.OutQuad);
            _tweenAnimation.Play();
            _tweenAnimation.OnComplete(() => { Observer.Emit(SokobanSampleEvent.MoveCubeByCharacterComplete); });
        }

        private void DefineMoveDelayDuration()
        {
            _moveDelayDuration = MoveDelayDuration + data.moveDelayDuration;
        }

        public void UpdateCube()
        {
            var cellCoordinate = data.cubePosition * SokobanSampleConfig.CellSize;
            var vector3 = new Vector3(cellCoordinate.x, _baseY, cellCoordinate.y);
            gameObject.transform.position = vector3;
        }

        private void ClearTween()
        {
            if (_tweenAnimation == null) return;
            _tweenAnimation.Kill();
            _tweenAnimation.onComplete = null;
            _tweenAnimation = null;
        }

        public void ResetCube()
        {
            var cellCoordinate = data.cubePosition * SokobanSampleConfig.CellSize;
            var vector3 = new Vector3(cellCoordinate.x, _baseY, cellCoordinate.y);
            gameObject.transform.position = vector3;
        }
    }
}