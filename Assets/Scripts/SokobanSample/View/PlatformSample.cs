﻿using SokobanSample.Config;
using SokobanSample.Model;
using UnityEngine;

namespace SokobanSample.View
{
    public class PlatformSample : MonoBehaviour
    {
        private static float _baseY;
        private static readonly int Color = Shader.PropertyToID("_Color");
        private static readonly int EmissionColor = Shader.PropertyToID("_EmissionColor");

        public PlatformData data;

        private void Awake()
        {
            _baseY = transform.position.y;
        }

        public void UpdatePlatform()
        {
            var cellCoordinate = data.platformPosition * SokobanSampleConfig.CellSize;
            var vector3 = new Vector3(cellCoordinate.x, _baseY, cellCoordinate.y);
            gameObject.transform.position = vector3;
        }

        public void ChangeColor()
        {
            if (data.activated)
            {
                gameObject.GetComponent<MeshRenderer>().material
                    .SetColor(Color, new Color(0.1960784f, 0.7843137f, 0.1960784f));
                gameObject.GetComponent<MeshRenderer>().material
                    .SetColor(EmissionColor, new Color(0.1960784f, 0.7843137f, 0.1960784f));
            }
            else
            {
                gameObject.GetComponent<MeshRenderer>().material
                    .SetColor(Color, new Color(0.7843137f, 0.1960784f, 0.1960784f));
                gameObject.GetComponent<MeshRenderer>().material
                    .SetColor(EmissionColor, new Color(0.7843137f, 0.1960784f, 0.1960784f));
            }
        }
    }
}