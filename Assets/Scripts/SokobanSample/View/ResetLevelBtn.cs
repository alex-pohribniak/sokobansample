﻿using Common.Singleton;
using SokobanSample.Enums;
using UnityEngine;

namespace SokobanSample.View
{
    public class ResetLevelBtn : MonoBehaviour
    {
        public void Reset()
        {
            Observer.Emit(SokobanSampleEvent.ResetLevel);
        }
    }
}