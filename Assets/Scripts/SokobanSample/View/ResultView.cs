﻿using Common.Enums;
using Common.Singleton;
using DG.Tweening;
using SokobanSample.Enums;
using UnityEngine;
using UnityEngine.UI;

namespace SokobanSample.View
{
    public class ResultView : MonoBehaviour
    {
        public Image result;
        private readonly SimpleDelegate _onShowResult;
        private Sequence _tweenAnimation;
        private float _tweenDuration;

        public ResultView()
        {
            _onShowResult = OnShowResult;
        }

        protected void Awake()
        {
            result.gameObject.SetActive(false);
            Observer.AddListener(SokobanSampleEvent.ShowResult, _onShowResult);
        }

        protected void OnDestroy()
        {
            ClearTween();
            if (Observer.IsNull()) return;
            Observer.RemoveListener(SokobanSampleEvent.ShowResult, _onShowResult);
        }

        private void ClearTween()
        {
            if (_tweenAnimation == null) return;
            _tweenAnimation.Kill();
            _tweenAnimation.onComplete = null;
            _tweenAnimation = null;
        }

        private void OnShowResult()
        {
            result.gameObject.SetActive(true);
            result.sprite = DefineResultSprite();
            ClearTween();
            _tweenDuration = 3f;
            _tweenAnimation = DOTween.Sequence();
            var tweenFade = result.DOFade(1f, _tweenDuration);
            _tweenAnimation.Join(tweenFade);
            _tweenAnimation.OnComplete(() => { Observer.Emit(SokobanSampleEvent.ShowResultComplete); });
            _tweenAnimation.Play();
        }

        private static Sprite DefineResultSprite()
        {
            if (MainModel.SokobanSampleData.win) return Resources.Load<Sprite>("Common/Sprites/Win");
            return !MainModel.SokobanSampleData.win
                ? Resources.Load<Sprite>("Common/Sprites/GameOver")
                : default;
        }
    }
}