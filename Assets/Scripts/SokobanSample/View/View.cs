﻿using System.Collections.Generic;
using Common.Enums;
using Common.Singleton;
using SokobanSample.Config;
using SokobanSample.Enums;
using UnityEngine;
using UnityEngine.UI;

namespace SokobanSample.View
{
    public class View : MonoBehaviour
    {
        public Character character;
        public CubeSample cubeSample;
        public PlatformSample platformSample;
        public WallSample wallSample;
        public Text moves;
        public Text pushes;

        private readonly SimpleDelegate _onInitView;
        private readonly SimpleDelegate _onMoveCharacter;
        private readonly SimpleDelegate _onMoveCubeByCharacter;
        private readonly SimpleDelegate _onResetLevelView;
        private readonly SimpleDelegate _onUpdateView;
        private List<CubeSample> _cubeSamples;
        private List<PlatformSample> _platformSamples;

        public View()
        {
            _onResetLevelView = OnResetLevelView;
            _onUpdateView = OnUpdateView;
            _onMoveCubeByCharacter = OnMoveCubeByCharacter;
            _onMoveCharacter = OnMoveCharacter;
            _onInitView = OnInitView;
        }

        private void Awake()
        {
            wallSample.gameObject.SetActive(false);
            platformSample.gameObject.SetActive(false);
            cubeSample.gameObject.SetActive(false);
            Observer.AddListener(SokobanSampleEvent.BuildLevel, _onInitView);
            Observer.AddListener(SokobanSampleEvent.MoveCharacter, _onMoveCharacter);
            Observer.AddListener(SokobanSampleEvent.MoveCubeByCharacter, _onMoveCubeByCharacter);
            Observer.AddListener(SokobanSampleEvent.UpdateView, _onUpdateView);
            Observer.AddListener(SokobanSampleEvent.ResetLevelView, _onResetLevelView);
        }

        private void OnDestroy()
        {
            if (Observer.IsNull()) return;
            Observer.RemoveListener(SokobanSampleEvent.BuildLevel, _onInitView);
            Observer.RemoveListener(SokobanSampleEvent.MoveCharacter, _onMoveCharacter);
            Observer.RemoveListener(SokobanSampleEvent.MoveCubeByCharacter, _onMoveCubeByCharacter);
            Observer.RemoveListener(SokobanSampleEvent.UpdateView, _onUpdateView);
            Observer.RemoveListener(SokobanSampleEvent.ResetLevelView, _onResetLevelView);
        }

        private void OnResetLevelView()
        {
            foreach (var cube in _cubeSamples) cube.ResetCube();

            character.ResetCharacter();
        }

        private void OnUpdateView()
        {
            foreach (var platform in _platformSamples) platform.ChangeColor();

            var characterData = MainModel.SokobanSampleData.character;
            moves.text = $"M:{characterData.moves}";
            pushes.text = $"P:{characterData.pushes}";
        }

        private void OnMoveCubeByCharacter()
        {
            foreach (var cube in _cubeSamples) cube.MoveCube();
        }

        private void OnMoveCharacter()
        {
            character.MoveCharacter();
        }

        private void OnInitView()
        {
            var walls = MainModel.SokobanSampleData.walls;
            foreach (var wall in walls)
            {
                var newWall = Instantiate(wallSample, wallSample.transform.parent);
                newWall.top.SetActive(wall.Value.top);
                newWall.bottom.SetActive(wall.Value.bottom);
                newWall.left.SetActive(wall.Value.left);
                newWall.right.SetActive(wall.Value.right);
                newWall.gameObject.SetActive(true);
                var cellCoordinate = wall.Key * SokobanSampleConfig.CellSize;
                newWall.transform.position = new Vector3(cellCoordinate.x, 0, cellCoordinate.y);
            }

            _platformSamples = new List<PlatformSample>();
            var platforms = MainModel.SokobanSampleData.platforms;
            foreach (var platform in platforms)
            {
                var newPlatform = Instantiate(platformSample, platformSample.transform.parent);
                newPlatform.data = platform;
                newPlatform.UpdatePlatform();
                _platformSamples.Add(newPlatform);
                newPlatform.gameObject.SetActive(true);
            }

            _cubeSamples = new List<CubeSample>();
            var cubes = MainModel.SokobanSampleData.cubes;
            foreach (var cube in cubes)
            {
                var newCube = Instantiate(cubeSample, cubeSample.transform.parent);
                newCube.data = cube;
                newCube.UpdateCube();
                _cubeSamples.Add(newCube);
                newCube.gameObject.SetActive(true);
            }

            var characterData = MainModel.SokobanSampleData.character;
            character.data = characterData;
        }
    }
}