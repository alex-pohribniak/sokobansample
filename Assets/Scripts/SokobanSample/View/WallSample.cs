﻿using UnityEngine;

namespace SokobanSample.View
{
    public class WallSample : MonoBehaviour
    {
        public GameObject top;
        public GameObject bottom;
        public GameObject left;
        public GameObject right;
    }
}
